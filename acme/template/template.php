<!DOCTYPE html>
<html lang="en">

<head>
    <title>Acme, Inc.</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="author" content="Erik Martinez">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:600%7CWendy+One" rel="stylesheet">
    <link rel="stylesheet" href="../css/main.css" media="screen" type="text/css">
    <link rel="stylesheet" href="../css/responsive.css" media="screen" type="text/css">
</head>

<body>
    <header id="site-brand-header">
        <div class="container">
            <div class="site-icon">
                <img src="../images/site/logo.gif" alt="site logo" class="siteIcon">
            </div>
            <div class="center">
            </div>
            <div class="account-icon">
                <img src="../images/site/account.gif" alt="my account" class="accountIcon">
                <p>My Account</p>
            </div>
        </div>
    </header>

    <nav id="site-nav">
        <?php include('../common/navigation.php'); ?>
    </nav>

    <main>
        <h1>Content goes here</h1>
    </main>

    <footer>
        <?php include('../common/footer.php'); ?>
    </footer>
</body>

</html>
<!-- 
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Acme, Inc.</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="author" content="Erik Martinez">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:600%7CWendy+One" rel="stylesheet">
    <link rel="stylesheet" href="../css/main.css" media="screen" type="text/css">
    <link rel="stylesheet" href="../css/responsive.css" media="screen" type="text/css">
</head>

<body>
    <header id="site-brand-header">
        <div class="container">
            <div class="site-icon">
                <a href="../index.php"><img src="../images/site/logo.gif" alt="site logo" class="siteIcon"></a>
            </div>
            <div class="center">
            </div>
            <div class="account-icon">
                <a href="../accounts/index.php?action=LogOut">LogOut</a>

            </div>
        </div>
        <?php if (isset($cookieFirstname) && isset($_SESSION['loggedin'])) {
            echo "<span class='welcomeCookie'>Welcome $cookieFirstname</span>";
        } else { } ?>
    </header>


    <nav id="site-nav">
        <?php
        //include('./common/navigation.php'); 
        echo $navigationList
        ?>
    </nav>

    <main>
        <h1>Content goes here</h1>
    </main>

    <footer>
        <?php include('../common/footer.php'); ?>
    </footer>
</body>

</html> -->
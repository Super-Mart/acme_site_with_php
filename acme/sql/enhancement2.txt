-- 1st - user insert
Use acme;

INSERT INTO `clients` 
(`clientId`
, `clientFirstname`
, `clientLastname`
, `clientEmail`
, `clientPassword`
, `clientLevel`
, `comments`) 

VALUES (NULL
, 'Tony'
, 'Stark'
, 'tony@starkent.com'
, 'Iam1ronM@n'
, '1'
, 'I am the real Ironman');

-- 2nd - user level update

Use acme;

UPDATE clients 
SET clientLevel = 3
WHERE clientFirstname = 'Tony';

-- 3rd - changing name of inventory item

UPDATE `inventory` 
SET invName = 'Climbing Rope',
    invDescription = REPLACE(invDescription, 'nylon rope', 'climbing rope') 
WHERE invName = 'Nylon Rope';

-- 4th 

SELECT inventory.invName , catagories.categoryName 
FROM catagories
    JOIN inventory ON inventory.categoryId = catagories.categoryId;

-- 5th

DELETE FROM `inventory` 
WHERE invName = 'Koenigsegg CCX Car';
-----------------------------------------------------------------------
-- 1st - user insert
START TRANSACTION;
Use acme;

INSERT INTO `clients` 
(`clientId`
, `clientFirstname`
, `clientLastname`
, `clientEmail`
, `clientPassword`
, `clientLevel`
, `comments`) 

VALUES (NULL
, 'Tony'
, 'Stark'
, 'tony@starkent.com'
, 'Iam1ronM@n'
, '1'
, 'I am the real Ironman');

-- 2nd - user level update

UPDATE clients 
SET clientLevel = 3
WHERE clientFirstname = 'Tony';

-- 3rd - changing name of inventory item

UPDATE `inventory` 
SET invName = 'Climbing Rope',
    invDescription = REPLACE(invDescription, 'nylon rope', 'climbing rope') 
WHERE invName = 'Nylon Rope';

-- 4th 

SELECT inventory.invName , categories.categoryName 
FROM categories
    JOIN inventory ON inventory.categoryId = categories.categoryId;

-- 5th

DELETE FROM `inventory` 
WHERE invName = 'Koenigsegg CCX Car';
ROLLBACK;
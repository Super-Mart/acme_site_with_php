<?php
/*
* Accounts Controller
*/

//Start session
session_start();

// Get the database connection file
require_once '../library/connections.php';
// Get newNav, check email, check password
require_once '../library/functions.php';
// Get the acme model for use as needed
require_once '../model/acme-model.php';
// Get the accounts model
require_once '../model/account-model.php';

// Get the array of categories
$categories = getCategories();
$navigationList = newNav($categories);

// Check if the firstname cookie exists, get its value
if (isset($_COOKIE['firstname'])) {
    $cookieFirstname = filter_input(INPUT_COOKIE, 'firstname', FILTER_SANITIZE_STRING);
}

// Get the value from the action name - value pair
$action = filter_input(INPUT_POST, 'action');
if ($action == NULL) {
    $action = filter_input(INPUT_GET, 'action');
}

switch ($action) {
        // Code to deliver the views will be here
    case 'MyAccount':
        include '../view/login.php';
        break;
    case 'AdminPage':
        include '../view/admin.php';
        break;
    case 'LoginTest':
        include '../view/admin.php';
        break;
    case 'Registration':
        include '../view/registration.php';
        break;
    case 'register':
        // Filter and store the data
        $clientFirstname = filter_input(INPUT_POST, 'clientFirstname', FILTER_SANITIZE_STRING);
        $clientLastname = filter_input(INPUT_POST, 'clientLastname', FILTER_SANITIZE_STRING);
        $clientEmail = filter_input(INPUT_POST, 'clientEmail', FILTER_SANITIZE_EMAIL);
        $clientPassword = filter_input(INPUT_POST, 'clientPassword', FILTER_SANITIZE_STRING);

        // echo "$clientFirstname,
        //  $clientLastname,
        //  $clientEmail,
        //  $clientPassword";
        // exit;

        // Check for existing email address in the table
        $existingEmail = checkExistingEmail($clientEmail);

        if ($existingEmail) {
            $message = '<p class="notice">That email address already exists. Do you want to login instead?</p>';
            include '../view/login.php';
            exit;
        }

        // Check for missing data
        if (empty($clientFirstname) || empty($clientLastname) || empty($clientEmail) || empty($clientPassword)) {
            $message = '<p class="warning">Please provide information for all empty form fields.</p>';
            include '../view/registration.php';
            exit;
        }

        //hash the checked password
        $hashedPassword = password_hash($clientPassword, PASSWORD_DEFAULT);

        // Send the data to the model
        $regOutcome = regClient($clientFirstname, $clientLastname, $clientEmail, $hashedPassword);

        // Check and report the result
        if ($regOutcome === 1) {
            // set cookie
            setcookie('firstname', $clientFirstname, strtotime('+1 year'), '/');

            $message = "<p class='success'>Thanks for registering $clientFirstname. Please use your email and password to login.</p>";
            include '../view/login.php';
            exit;
        } else {
            $message = "<p class='notice'>Sorry $clientFirstname, but the registration failed. Please try again.</p>";
            include '../view/registration.php';
            exit;
        }
    case 'Login':
        $clientEmail = filter_input(INPUT_POST, 'clientEmail', FILTER_SANITIZE_EMAIL);
        $clientEmail = checkEmail($clientEmail);
        $clientPassword = filter_input(INPUT_POST, 'clientPassword', FILTER_SANITIZE_STRING);
        $passwordCheck = checkPassword($clientPassword);

        // Run basic checks, return if errors
        if (empty($clientEmail) || empty($passwordCheck)) {
            $message = "<p class='notice'>Please provide a valid email address and password.</p>";
            include '../view/login.php ';
            exit;
        }

        // A valid password exists, proceed with the login process
        // Query the client data based on the email address
        $clientData = getClient($clientEmail);
        // Compare the password just submitted against
        // the hashed password for the matching client
        $hashCheck = password_verify($clientPassword, $clientData['clientPassword']);
        // If the hashes don't match create an error
        // and return to the login view
        if (!$hashCheck) {
            $message = '<p class="notice">Please check your password and try again.</p>';
            include '../view/login.php';
            exit;
        }
        // A valid user exists, log them in
        $_SESSION['loggedin'] = TRUE;
        // Remove the password from the array
        // the array_pop function removes the last
        // element from an array
        array_pop($clientData);
        // Store the array into the session        

        $_SESSION['clientData'] = $clientData;
        $clientFirstname = $clientData['clientFirstname'];
        setcookie('firstname', $clientFirstname,  time() - 60 * 60 * 24 * 365, '/');
        // Send them to the admin view
        include '../view/admin.php';
        exit;
    case 'LogOut':
        session_unset();
        session_destroy();
        header("location: /acme/");
        exit;
    case 'mod-account':
        include '../view/client-update.php';
        exit;
    case 'updateAccount':
        // Filter and store the data
        $clientFirstname = filter_input(INPUT_POST, 'clientFirstname', FILTER_SANITIZE_STRING);
        $clientLastname = filter_input(INPUT_POST, 'clientLastname', FILTER_SANITIZE_STRING);
        $clientEmail = filter_input(INPUT_POST, 'clientEmail', FILTER_SANITIZE_EMAIL);
        $clientId = filter_input(INPUT_POST, 'clientId', FILTER_SANITIZE_NUMBER_INT);

        $checkedEmail = checkEmail($clientEmail);

        if ($_SESSION['clientData']['clientEmail'] != $clientEmail) {
            if (checkExistingEmail($clientEmail)) {
                $message = "<p class='warning'>That email is already in use! Pick another.</P>";
                include '../view/client-update.php';
                exit;
            }
        }
        // Check for missing data
        if (empty($clientFirstname) || empty($clientLastname) || empty($clientEmail)) {
            $message = '<p class="warning">Please provide information for all empty form fields.</p>';
            include '../view/client-update.php';
            exit;
        }
        $updatedResult = updateAccount(
            $clientFirstname,
            $clientLastname,
            $checkedEmail,
            $clientId
        );
        // Check and report the result
        if ($updatedResult) {
            $message = "<p class='success'>Congratulations, your account was successfully updated.</p>";
            $_SESSION['message'] = $message;
            $_SESSION['clientData'] = getClient($clientEmail);
            include '../view/admin.php';
            exit;
        } else {
            $message = "<p class='warning'>Error. Could not update account information.</p>";
            include '../view/admin.php';
            exit;
        }
        break;
    case 'updatePassword':
        // Filter and store the data
        $clientPassword = filter_input(INPUT_POST, 'clientPassword', FILTER_SANITIZE_STRING);
        $clientId = filter_input(INPUT_POST, 'clientId', FILTER_SANITIZE_NUMBER_INT);

        $checkPassword = checkPassword($clientPassword);

        // Check for missing data
        if (empty($checkPassword)) {
            $message = '<p class="warning">Please enter a valid password.</p>';
            include '../view/client-update.php';
            exit;
        }
        $hashedPassword = password_hash($clientPassword, PASSWORD_DEFAULT);
        $updatedPassword = updatePassword(
            $hashedPassword,
            $clientId
        );
        // Check and report the result
        if ($updatedPassword) {
            $message = "<p class='success'>Congratulations, your password was successfully updated.</p>";
            $_SESSION['message'] = $message;
            //$_SESSION['clientData'] = getClient($clientEmail);
            include '../view/admin.php';
            exit;
        } else {
            $message = "<p class='warning'>Error. Could not update account information.</p>";
            include '../view/client-update.php';
            exit;
        }
        break;
    default:
        include '../view/login.php';
        exit;
}

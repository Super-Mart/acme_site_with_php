<?php
/*
 * Acme controller
 */

//Start session
session_start();

require_once './library/connections.php';

require_once './model/acme-model.php';

require_once './library/functions.php';

require_once './model/product-model.php';

$categories = getCategories();
$navList = newNav($categories);
//$products = getProducts();
//var_dump($categories);
//var_dump($products);
//exit;

$action = filter_input(INPUT_POST, 'action');
if ($action == NULL) {
    $action = filter_input(INPUT_GET, 'action');
}

// Check if the firstname cookie exists, get its value
if (isset($_COOKIE['firstname'])) {
    $cookieFirstname = filter_input(INPUT_COOKIE, 'firstname', FILTER_SANITIZE_STRING);
}

switch ($action) {
    case 'template':
        include './template/home.php';
        break;
    default:
        $featProd = getFeatured();
        $featProdDisplay = buildProductFeature($featProd);
        include './view/home.php';
}

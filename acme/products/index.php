<?php
/*
* Products Controller
*/

//Start session
session_start();

// Get the database connection file
require_once '../library/connections.php';
// Get newNav
require_once '../library/functions.php';
// Get the acme model for use as needed
require_once '../model/acme-model.php';
// Get the product model
require_once '../model/product-model.php';
// Get the uploads model
require_once '../model/uploads-model.php';

// Get the array of categories
$categories = getCategories();
$navigationList = newNav($categories);

//var_dump($navigationList);

// Get the value from the action name - value pair
$action = filter_input(INPUT_POST, 'action');
if ($action == NULL) {
    $action = filter_input(INPUT_GET, 'action');
}

// Check if the firstname cookie exists, get its value
if (isset($_COOKIE['firstname'])) {
    $cookieFirstname = filter_input(INPUT_COOKIE, 'firstname', FILTER_SANITIZE_STRING);
}
switch ($action) {
        // Code to deliver the views will be here
        // case 'cancel':
        //     include header("location: /acme/products");
        //     break;
    case 'new-cat':
        include '../view/new-cat.php';
        break;
    case 'new-prod':
        include '../view/new-prod.php';
        break;
    case 'addNewProduct':
        // Filter and store the data
        $invName = filter_input(INPUT_POST, 'invName', FILTER_SANITIZE_STRING);
        $invDescription = filter_input(INPUT_POST, 'invDescription', FILTER_SANITIZE_STRING);
        $invImage = filter_input(INPUT_POST, 'invImage', FILTER_SANITIZE_URL);
        $invThumbnail = filter_input(INPUT_POST, 'invThumbnail', FILTER_SANITIZE_URL);
        $invPrice = filter_input(INPUT_POST, 'invPrice', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
        $invStock = filter_input(INPUT_POST, 'invStock', FILTER_SANITIZE_NUMBER_INT);
        $invSize = filter_input(INPUT_POST, 'invSize', FILTER_SANITIZE_NUMBER_INT);
        $invWeight = filter_input(INPUT_POST, 'invWeight', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
        $invLocation = filter_input(INPUT_POST, 'invLocation', FILTER_SANITIZE_STRING);
        $categoryId = filter_input(INPUT_POST, 'categoryId', FILTER_SANITIZE_NUMBER_INT);
        $invVendor = filter_input(INPUT_POST, 'invVendor', FILTER_SANITIZE_STRING);
        $invStyle = filter_input(INPUT_POST, 'invStyle', FILTER_SANITIZE_STRING);

        // echo "$invName,
        //     $invDescription,
        //     $invImage,
        //     $invThumbnail,
        //     $invPrice,
        //     $invStock,
        //     $invSize,
        //     $invWeight,
        //     $invLocation,
        //     $categoryId,
        //     $invVendor,
        //     $invStyle";
        // exit;

        // Check for missing data
        if (
            empty($invName) || empty($invDescription) || empty($invImage) || empty($invThumbnail)
            || empty($invPrice) || empty($invStock) || empty($invSize) || empty($invWeight)
            || empty($invLocation) || empty($categoryId) || empty($invVendor) || empty($invStyle)
        ) {
            $message = '<p class="warning">*Please provide information for all empty form fields.</p>';
            include '../view/new-prod.php';
            exit;
        }

        // Send the data to the model
        $newProdOutcome = addNewProduct(
            $invName,
            $invDescription,
            $invImage,
            $invThumbnail,
            $invPrice,
            $invStock,
            $invSize,
            $invWeight,
            $invLocation,
            $categoryId,
            $invVendor,
            $invStyle
        );

        // Check and report the result
        if ($newProdOutcome === 1) {
            $message = "<p class='success'>$invName has been added to Products.</p>";
            $_POST = array();
            $_SESSION['message'] = $message;
            header('location: /acme/products/');
            exit;
        } else {
            $message = "<p class='warning'>Sorry, but the $invName cannot be added. Please try again.</p>";
            include '../view/new-prod.php';
            exit;
        }
    case 'addNewCategory':
        // Filter and store the data
        $categoryName = filter_input(INPUT_POST, 'categoryName', FILTER_SANITIZE_STRING);

        // echo "$categoryName";
        // exit;

        // Check for missing data
        if (empty($categoryName)) {
            $message = '<p class="warning">*Please provide information for all empty form fields.</p>';
            include '../view/new-cat.php';
            exit;
        }

        // Send the data to the model
        $newCatOutcome = addNewCategory(
            $categoryName
        );

        // Check and report the result
        if ($newCatOutcome === 1) {
            // $message = "<p>$categoryName has been added to Categories.</p>";
            include '../view/prod-mgmt.php';
            exit;
        } else {
            $message = "<p>Sorry, but $categoryName cannot be added.</p>";
            include '../view/new-cat.php';
            exit;
        }

        /* *********************************** 
        * Get Inventory Items by categoryId 
        * Used for starting Update & delete process 
        * ********************************** */
    case 'getInventoryItems':
        // Get the categoryId 
        $categoryId = filter_input(INPUT_GET, 'categoryId', FILTER_SANITIZE_NUMBER_INT);
        // Fetch the products by categoryId from the DB 
        $productsArray = getProductsByCategory($categoryId);
        // Convert the array to a JSON object and send it back 
        echo json_encode($productsArray);
        break;
        // Working correctly
    case 'mod':
        $invId = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
        $prodInfo = getProductInfo($invId);
        if (count($prodInfo) < 1) {
            $message = 'Sorry, no product information could be found.';
        }
        include '../view/prod-update.php';
        exit;
        break;
    case 'updateProd':
        // Filter and store the data
        $invName = filter_input(INPUT_POST, 'invName', FILTER_SANITIZE_STRING);
        $invDescription = filter_input(INPUT_POST, 'invDescription', FILTER_SANITIZE_STRING);
        $invImage = filter_input(INPUT_POST, 'invImage', FILTER_SANITIZE_URL);
        $invThumbnail = filter_input(INPUT_POST, 'invThumbnail', FILTER_SANITIZE_URL);
        $invPrice = filter_input(INPUT_POST, 'invPrice', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
        $invStock = filter_input(INPUT_POST, 'invStock', FILTER_SANITIZE_NUMBER_INT);
        $invSize = filter_input(INPUT_POST, 'invSize', FILTER_SANITIZE_NUMBER_INT);
        $invWeight = filter_input(INPUT_POST, 'invWeight', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
        $invLocation = filter_input(INPUT_POST, 'invLocation', FILTER_SANITIZE_STRING);
        $categoryId = filter_input(INPUT_POST, 'categoryId', FILTER_SANITIZE_NUMBER_INT);
        $invVendor = filter_input(INPUT_POST, 'invVendor', FILTER_SANITIZE_STRING);
        $invStyle = filter_input(INPUT_POST, 'invStyle', FILTER_SANITIZE_STRING);
        $invId = filter_input(INPUT_POST, 'invId', FILTER_SANITIZE_NUMBER_INT);
        // echo "$invName,
        //     $invDescription,
        //     $invImage,
        //     $invThumbnail,
        //     $invPrice,
        //     $invStock,
        //     $invSize,
        //     $invWeight,
        //     $invLocation,
        //     $categoryId,
        //     $invVendor,
        //     $invStyle";
        // exit;

        // Check for missing data
        if (
            empty($invName) || empty($invDescription) || empty($invImage) || empty($invThumbnail) || empty($invPrice) || empty($invStock) || empty($invSize) || empty($invWeight)
            || empty($invLocation) || empty($categoryId) || empty($invVendor) || empty($invStyle)
        ) {
            $message = '<p class="warning">Please complete all information for the updated item! Double check the category of the item.</p>';
            include '../view/prod-update.php';
            exit;
        }

        // Send the data to the model
        $updateResult = updateProduct(
            $invName,
            $invDescription,
            $invImage,
            $invThumbnail,
            $invPrice,
            $invStock,
            $invSize,
            $invWeight,
            $invLocation,
            $categoryId,
            $invVendor,
            $invStyle,
            $invId
        );

        // Check and report the result
        if (updateResult) {
            $message = "<p class='success'>Congratulations, $invName was successfully updated.</p>";
            $_SESSION['message'] = $message;
            header('location: /acme/products/');
            exit;
        } else {
            $message = "<p class='warning'>Error. The updated product could not added.</p>";
            include '../view/prod-update.php';
            exit;
        }
        break;
    case 'del':
        $invId = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
        $prodInfo = getProductInfo($invId);
        if (count($prodInfo) < 1) {
            $message = 'Sorry, no product information could be found.';
        }
        include '../view/prod-delete.php';
        exit;
        break;
    case 'deleteProd':
        // Filter and store the data
        $invName = filter_input(INPUT_POST, 'invName', FILTER_SANITIZE_STRING);
        // $invDescription = filter_input(INPUT_POST, 'invDescription', FILTER_SANITIZE_STRING);        
        $invId = filter_input(INPUT_POST, 'invId', FILTER_SANITIZE_NUMBER_INT);

        // Send the data to the model
        $deleteResult = deleteProduct(
            $invId
        );

        // Check and report the result
        if (deleteResult) {
            $message = "<p class='success'>Congratulations, $invName was successfully deleted.</p>";
            $_SESSION['message'] = $message;
            header('location: /acme/products/');
            exit;
        } else {
            $message = "<p class='warning'>Error: $invName could not be deleted.</p>";
            $_SESSION['message'] = $message;
            header('location: /acme/products/');
            exit;
        }
        break;
    case 'category':
        $categoryName = filter_input(INPUT_GET, 'categoryName', FILTER_SANITIZE_STRING);

        $products = getProductsByCategoryName($categoryName);

        if (!count($products)) {
            $message = "<p class='notice'>Sorry, no $categoryName products could be found.</p>";
        } else {
            $prodDisplay = buildProductsDisplay($products);
        }
        //echo $prodDisplay;
        include '../view/category.php';
        break;
    case 'productDetail':
        $invId = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING);

        $getThumbnails = getThumbnails($invId);

        $product = getProductInfo($invId);
        // var_dump($product);
        // echo $invId;
        if (!count($product)) {
            $message = "<p class='notice'>Sorry product could be found.</p>";
        } else {
            $prodDisplay = buildProductsDetail($product);
            $thumbDisplay = buildThumbDetail($getThumbnails);
        }

        include '../view/product-detail.php';
        exit;
    case 'fea':
        $invId = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
        $featProd = getFeatured();
        $result = updateFeatured($featProd['invId']);

        if ($result) {
            $_SESSION['message'] = '<p class="success">' . $featProd['invName'] . 'product was successfully unset</p>';
            $newFeatProd = setFeatProd($invId);
        } else {
            $_SESSION['message'] = '<p class="warning">' . $featProd['invName'] . 'featured product was unsuccessful</p>';
        }
        header("location: /acme/products/");
        exit;
    default:
        $categoryList = buildCategoryList($categories);
        include '../view/prod-mgmt.php';
        exit;
}

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Acme, Inc.</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="author" content="Erik Martinez">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:60%7CWendy+One" rel="stylesheet">
    <link rel="stylesheet" href="./css/main.css" media="screen" type="text/css">
    <link rel="stylesheet" href="./css/responsive.css" media="screen" type="text/css">
</head>

<body>
    <header id="site-brand-header">
        <div class="container">
            <div class="site-icon">
                <a href="./index.php?action=Home"><img src="./images/site/logo.gif" alt="site logo" class="siteIcon"></a>
            </div>
            <div class="center">

            </div>
            <div class="account-icon">
                <?php if (isset($_SESSION['loggedin'])) {
                    echo "<a href='./accounts/index.php?action=LogOut'>LogOut</a>";
                    echo "<a class='welcomeCookie' href='./accounts/index.php?action=AdminPage'>Welcome " . $_SESSION['clientData']['clientFirstname'] . "</a>";
                } else {
                    echo "<a href='./accounts/index.php?action=MyAccount'><img src='./images/site/account.gif' alt='my account' class='accountIcon'> </a> <p class='myAccountText'>My Account</p>";
                } ?>

            </div>
        </div>
        <?php if (isset($cookieFirstname) && isset($_SESSION['loggedin'])) {
            echo "<a class='welcomeCookie' href='../accounts/index.php?action=AdminPage'>Welcome $cookieFirstname</a>";
        } else { } ?>
    </header>

    <nav id="site-nav">
        <?php
        //include('./common/navigation.php'); 
        echo $navList;
        ?>
    </nav>

    <main>
        <!-- welcome section -->
        <section class="welcome-section">
            <h1>Welcome to Acme!</h1>
            <?php if (isset($featProdDisplay)) {
                echo $featProdDisplay;
            }
            ?>
            <!-- <div class="img-main">
                <img src="./images/site/rocketfeature.jpg" alt="" width="100%" class="test-img">
                <div class="acme-rocket-description">
                    <ul>
                        <li>
                            <h2>Acme Rocket</h2>
                        </li>
                        <li>Quick lighting fuse</li>
                        <li>NHTSA approved seat belts</li>
                        <li>Mobile launch stand included</li>
                        <li><a href="./acme/cart/"><img id="iwantit" alt="Add to cart button" src="./images/site/iwantit.gif"></a></li>
                    </ul>
                </div>
            </div> -->
        </section>
        <!-- two part section -->
        <section class="two-part-section">
            <!-- <div class="review-section">
                <h1>Acme Rocket Review</h1>
                <ul>
                    <li>"I don't know how I ever caught roadrunners before this." (4/5)</li>
                    <li>"That thing was fast!" (4/5)</li>
                    <li>"Talk about fast delivery." (5/5)</li>
                    <li>"I didn't even have to pull the meat apart." (4.5/5)</li>
                    <li>"I'm on my thirtieth one. I love these things!" (5/5)</li>
                </ul>
            </div> -->

            <div class="featured-recipe-section">
                <h1>Featured Recipes</h1>
                <div class="recipe-images-text">
                    <figure>
                        <div class="img-style">
                            <img src="./images/recipes/bbqsand.jpg" alt="Tea cup with steam and pen on bed">
                        </div>
                        <figcaption>Pulled Roadrunner BBQ</figcaption>
                    </figure>
                    <figure>
                        <div class="img-style">
                            <img src="./images/recipes/potpie.jpg" alt="Tea cup with steam and pen on bed">
                        </div>
                        <figcaption>Roadrunner Pot Pie</figcaption>
                    </figure>
                    <figure>
                        <div class="img-style">
                            <img src="./images/recipes/soup.jpg" alt="Tea cup with steam and pen on bed">
                        </div>
                        <figcaption>Roadrunner Soup</figcaption>
                    </figure>
                    <figure>
                        <div class="img-style">
                            <img src="./images/recipes/taco.jpg" alt="Tea cup with steam and pen on bed">
                        </div>
                        <figcaption>Roadrunner Tacos</figcaption>
                    </figure>
                </div>
            </div>
        </section>
    </main>

    <footer>
        <?php include('./common/footer.php'); ?>
    </footer>
</body>

</html>
<?php
$catList = '<select name="categoryId" id="categoryId">';
$catList .= "<option value=' ' selected disabled>Select category</option>";
foreach ($categories as $category) {
    $catList .= "<option value='$category[categoryId]'";
    if (isset($catType)) {
        if ($category['categoryId'] === $catType) {
            $catList .= ' selected ';
        }
    }
    $catList .= ">$category[categoryName]</option>";
}
$catList .= '</select>';


if ($_SESSION['clientData']['clientLevel'] < 2) {
    header('location: /acme/');
    exit;
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Acme, Inc.</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="author" content="Erik Martinez">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:600%7CWendy+One" rel="stylesheet">
    <link rel="stylesheet" href="../css/main.css" media="screen" type="text/css">
    <link rel="stylesheet" href="../css/responsive.css" media="screen" type="text/css">
</head>

<body>
    <header id="site-brand-header">
        <?php include('../common/header.php'); ?>
    </header>

    <nav id="site-nav">
        <?php
        //include('../common/navigation.php'); 
        echo $navigationList
        ?>
    </nav>

    <main>
        <?php
        if (isset($message)) {
            echo $message;
        }
        ?>
        <form method="post" action="/acme/products/index.php" class="basic">
            <fieldset>
                <label for="categoryId">Category of New Product:</label>
                <br>
                <?php
                echo $catList;
                ?>
                <br>
                <label for="invName">Name of New Product:</label>
                <br>
                <input type="text" name="invName" id="invName" value="<?php if (isset($_POST['invName'])) echo $_POST['invName']; ?>" required oninvalid="this.setCustomValidity('Please Enter Product Name')" oninput="this.setCustomValidity('')">
                <br>
                <label for="invDescription">New Product Description:</label>
                <br>
                <textarea name="invDescription" id="invDescription" required oninvalid="this.setCustomValidity('Please Enter a valid Description')" oninput="this.setCustomValidity('')"><?php if (isset($_POST['invDescription'])) echo $_POST['invDescription']; ?></textarea>
                <br>
                <label for="invImage">Image Path:</label>
                <br>
                <input type="text" name="invImage" id="invImage" value="/acme/images/products/no-image.png" required>
                <br>
                <label for="invThumbnail">Thumbnail Path:</label>
                <br>
                <input type="text" name="invThumbnail" id="invThumbnail" value="/acme/images/products/no-image.png" required>
                <br>
                <label for="invPrice">New Product Price:</label>
                <br>
                <input type="number" name="invPrice" id="invPrice" step="any" value="<?php if (isset($_POST['invPrice'])) echo $_POST['invPrice']; ?>" required oninvalid="this.setCustomValidity('Not a Valid Price')" oninput="this.setCustomValidity('')">
                <br>
                <label for="invStock">Stock:</label>
                <br>
                <input type="number" name="invStock" id="invStock" step="any" value="<?php if (isset($_POST['invStock'])) echo $_POST['invStock']; ?>" required oninvalid="this.setCustomValidity('Please Enter Stock')" oninput="this.setCustomValidity('')">
                <br>
                <label for="invSize">Size (Total dimensions):</label>
                <br>
                <input type="number" name="invSize" id="invSize" step="any" value="<?php if (isset($_POST['invSize'])) echo $_POST['invSize']; ?>" required oninvalid="this.setCustomValidity('Not a Valid Size')" oninput="this.setCustomValidity('')">
                <br>
                <label for="invWeight">Weight (lbs):</label>
                <br>
                <input type="number" name="invWeight" id="invWeight" step="any" value="<?php if (isset($_POST['invWeight'])) echo $_POST['invWeight']; ?>" required oninvalid="this.setCustomValidity('Not a Valid Weight')" oninput="this.setCustomValidity('')">
                <br>
                <label for="invLocation">Location:</label>
                <br>
                <input type="text" name="invLocation" id="invLocation" value="<?php if (isset($_POST['invLocation'])) echo $_POST['invLocation']; ?>" required oninvalid="this.setCustomValidity('Please Provide a Location')" oninput="this.setCustomValidity('')">
                <br>
                <label for="invVendor">Vendor Name:</label>
                <br>
                <input type="text" name="invVendor" id="invVendor" value="<?php if (isset($_POST['invVendor'])) echo $_POST['invVendor']; ?>" required oninvalid="this.setCustomValidity('Please Provide Vendor')" oninput="this.setCustomValidity('')">
                <br>
                <label for="invStyle">Product Style:</label>
                <br>
                <input type="text" name="invStyle" id="invStyle" value="<?php if (isset($_POST['invStyle'])) echo $_POST['invStyle']; ?>" required oninvalid="this.setCustomValidity('Please Provide Style of Product')" oninput="this.setCustomValidity('')">
                <br>
                <button type="submit" name="submit" id="newprodbtn" value="addNewProduct">Add New Product!</button>
                <!-- <input type="submit" name="submit" id="regbtn" value="Register" class="a-btn"> -->
                <!-- Add the action name - value pair -->
                <input type="hidden" name="action" value="addNewProduct">

                <button onclick="history.go(-1);">Cancel</button>
            </fieldset>
        </form>
    </main>

    <footer>
        <?php include('../common/footer.php'); ?>
    </footer>
</body>

</html>
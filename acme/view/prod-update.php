<?php
// Build the categories option list
$catList = '<select name="categoryId" id="categoryId">';
//$catList .= "<option value=' ' selected disabled>Select category</option>";
foreach ($categories as $category) {
    $catList .= "<option value='$category[categoryId]'";
    if (isset($catType)) {
        if ($category['categoryId'] === $catType) {
            $catList .= ' selected ';
        }
    } elseif (isset($prodInfo['categoryId'])) {
        if ($category['categoryId'] === $prodInfo['categoryId']) {
            $catList .= ' selected ';
        }
    }
    $catList .= ">$category[categoryName]</option>";
}
$catList .= '</select>';

if ($_SESSION['clientData']['clientLevel'] < 2) {
    header('location: /acme/');
    exit;
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title><?php if (isset($prodInfo['invName'])) {
                echo "Modify $prodInfo[invName] ";
            } elseif (isset($invName)) {
                echo $invName;
            } ?> | Acme, Inc.</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="author" content="Erik Martinez">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:600%7CWendy+One" rel="stylesheet">
    <link rel="stylesheet" href="../css/main.css" media="screen" type="text/css">
    <link rel="stylesheet" href="../css/responsive.css" media="screen" type="text/css">
</head>

<body>
    <header id="site-brand-header">
        <?php include('../common/header.php'); ?>
    </header>

    <nav id="site-nav">
        <?php
        //include('../common/navigation.php'); 
        echo $navigationList
        ?>
    </nav>

    <main>
        <?php
        if (isset($message)) {
            echo $message;
        }
        ?>
        <h1><?php if (isset($prodInfo['invName'])) {
                echo "Modify $prodInfo[invName] ";
            } elseif (isset($invName)) {
                echo $invName;
            } ?></h1>
        <form method="post" action="/acme/products/index.php" class="basic">
            <fieldset>
                <label for="categoryId">Category of New Product:</label>
                <br>
                <?php
                echo $catList;
                ?>
                <br>
                <label for="invName">Name of New Product:</label>
                <br>
                <input 
                type="text" 
                name="invName" 
                id="invName" <?php if (isset($invName)) {
                                        echo "value='$invName'";
                                    } elseif (isset($prodInfo['invName'])) {
                                              echo "value='$prodInfo[invName]'";
                                    } ?> 
                required 
                oninvalid="this.setCustomValidity('Please Enter Product Name')" 
                oninput="this.setCustomValidity('')">
                <br>
                <label for="invDescription">New Product Description:</label>
                <br>
                <textarea 
                name="invDescription" 
                id="invDescription" 
                required 
                oninvalid="this.setCustomValidity('Please Enter a valid Description')" 
                oninput="this.setCustomValidity('')"><?php if (isset($invDescription)) {
                                                                echo $invDescription;
                                                            } elseif (isset($prodInfo['invDescription'])) {
                                                                      echo $prodInfo['invDescription'];
                                                            } ?></textarea>
                <br>
                <label for="invImage">Image Path:</label>
                <br>
                <input type="text" name="invImage" id="invImage" <?php if (isset($invImage)) {
                                                                        echo "value='$invImage'";
                                                                    } elseif (isset($prodInfo['invImage'])) {
                                                                        echo "value='$prodInfo[invImage]'";
                                                                    } ?> required>
                <br>
                <label for="invThumbnail">Thumbnail Path:</label>
                <br>
                <input type="text" name="invThumbnail" id="invThumbnail" <?php if (isset($invThumbnail)) {
                                                                                echo "value='$invThumbnail'";
                                                                            } elseif (isset($prodInfo['invThumbnail'])) {
                                                                                echo "value='$prodInfo[invThumbnail]'";
                                                                            } ?> required>
                <br>
                <label for="invPrice">New Product Price:</label>
                <br>
                <input type="number" name="invPrice" id="invPrice" step="any" <?php if (isset($invPrice)) {
                                                                                    echo "value='$invPrice'";
                                                                                } elseif (isset($prodInfo['invPrice'])) {
                                                                                    echo "value='$prodInfo[invPrice]'";
                                                                                } ?> required oninvalid="this.setCustomValidity('Not a Valid Price')" oninput="this.setCustomValidity('')">
                <br>
                <label for="invStock">Stock:</label>
                <br>
                <input type="number" name="invStock" id="invStock" step="any" <?php if (isset($invStock)) {
                                                                                    echo "value='$invStock'";
                                                                                } elseif (isset($prodInfo['invStock'])) {
                                                                                    echo "value='$prodInfo[invStock]'";
                                                                                } ?> required oninvalid="this.setCustomValidity('Please Enter Stock')" oninput="this.setCustomValidity('')">
                <br>
                <label for="invSize">Size (Total dimensions):</label>
                <br>
                <input type="number" name="invSize" id="invSize" step="any" <?php if (isset($invSize)) {
                                                                                echo "value='$invSize'";
                                                                            } elseif (isset($prodInfo['invSize'])) {
                                                                                echo "value='$prodInfo[invSize]'";
                                                                            } ?> required oninvalid="this.setCustomValidity('Not a Valid Size')" oninput="this.setCustomValidity('')">
                <br>
                <label for="invWeight">Weight (lbs):</label>
                <br>
                <input type="number" name="invWeight" id="invWeight" step="any" <?php if (isset($invWeight)) {
                                                                                    echo "value='$invWeight'";
                                                                                } elseif (isset($prodInfo['invWeight'])) {
                                                                                    echo "value='$prodInfo[invWeight]'";
                                                                                } ?> required oninvalid="this.setCustomValidity('Not a Valid Weight')" oninput="this.setCustomValidity('')">
                <br>
                <label for="invLocation">Location:</label>
                <br>
                <input type="text" name="invLocation" id="invLocation" <?php if (isset($invLocation)) {
                                                                            echo "value='$invLocation'";
                                                                        } elseif (isset($prodInfo['invLocation'])) {
                                                                            echo "value='$prodInfo[invLocation]'";
                                                                        } ?> required oninvalid="this.setCustomValidity('Please Provide a Location')" oninput="this.setCustomValidity('')">
                <br>
                <label for="invVendor">Vendor Name:</label>
                <br>
                <input type="text" name="invVendor" id="invVendor" <?php if (isset($invVendor)) {
                                                                        echo "value='$invVendor'";
                                                                    } elseif (isset($prodInfo['invVendor'])) {
                                                                        echo "value='$prodInfo[invVendor]'";
                                                                    } ?> required oninvalid="this.setCustomValidity('Please Provide Vendor')" oninput="this.setCustomValidity('')">
                <br>
                <label for="invStyle">Product Style:</label>
                <br>
                <input type="text" name="invStyle" id="invStyle" <?php if (isset($invStyle)) {
                                                                        echo "value='$invStyle'";
                                                                    } elseif (isset($prodInfo['invStyle'])) {
                                                                        echo "value='$prodInfo[invStyle]'";
                                                                    } ?> required oninvalid="this.setCustomValidity('Please Provide Style of Product')" oninput="this.setCustomValidity('')">
                <br>
                <button type="submit" name="submit" id="newprodbtn" value="updateProd">Update Product</button>

                <input type="hidden" name="action" value="updateProd">

                <input type="hidden" name="invId" value="<?php if (isset($prodInfo['invId'])) {
                                                                echo $prodInfo['invId'];
                                                            } elseif (isset($invId)) {
                                                                echo $invId;
                                                            } ?>">

                <!-- <button onclick="history.go(-1);">Cancel</button> -->
            </fieldset>
        </form>
    </main>

    <footer>
        <?php include('../common/footer.php'); ?>
    </footer>
</body>

</html>
<?php 
if ($_SESSION['clientData']['clientLevel'] < 2) {
    header("location: /acme/");
    exit;
}

if (isset($_SESSION['message'])) {
    $message = $_SESSION['message'];
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Acme, Inc.</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="author" content="Erik Martinez">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:600%7CWendy+One" rel="stylesheet">
    <link rel="stylesheet" href="../css/main.css" media="screen" type="text/css">
    <link rel="stylesheet" href="../css/responsive.css" media="screen" type="text/css">
</head>

<body>
    <header id="site-brand-header">
        <?php include('../common/header.php'); ?>
    </header>

    <nav id="site-nav">
        <?php
        //include('./common/navigation.php'); 
        echo $navigationList;
        ?>
    </nav>

    <main>
        <div class="new-prodmgmt-container">
            <h1 class="little-right">Product Management</h1>
            <p class="little-right">Welcome to the Products Management page! To get started please choose an option below.</p>
            <ul>
                <li><a href="../products/index.php?action=new-cat" class="nc-btn">Add a new Category</a></li>
                <li><a href="../products/index.php?action=new-prod" class="np-btn">Add a new Inventory Item</a></li>
            </ul>


            <?php
            if (isset($message)) {
                echo $message;
            }
            if (isset($categoryList)) {
                echo '<h2 class="little-right">Products By Category</h2>';
                echo '<p class="little-right">Choose a category to see those products</p>';
                echo $categoryList;
            }
            ?>
            <noscript>
                <p><strong>JavaScript Must Be Enabled to Use this Page.</strong></p>
            </noscript>

            <table id="productsDisplay"></table>
        </div>
    </main>

    <footer>
        <?php include('../common/footer.php'); ?>
    </footer>

    <script src="../js/products.js"></script>
</body>

</html>
<?php unset($_SESSION['message']); ?>
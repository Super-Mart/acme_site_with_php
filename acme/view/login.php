<!DOCTYPE html>
<html lang="en">

<head>
    <title>Acme, Inc.</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="author" content="Erik Martinez">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:600%7CWendy+One" rel="stylesheet">
    <link rel="stylesheet" href="../css/main.css" media="screen" type="text/css">
    <link rel="stylesheet" href="../css/responsive.css" media="screen" type="text/css">
</head>

<body>
    <header id="site-brand-header">
        <?php include('../common/header.php'); ?>
    </header>

    <nav id="site-nav">
        <?php
        echo $navigationList
        ?>
    </nav>

    <main>
        <?php
        if (isset($message)) {
            echo $message;
        }
        ?>
        <form method="post" action="/acme/accounts/" class="basic">
            <fieldset>
                <h1 class="acme-login-heading">Acme Login</h1>
                <label for="clientEmail">
                    <span class="span-label">Email:</span>
                    <input id="clientEmail" type="email" name="clientEmail" placeholder="Your Email Address" required />
                </label>

                <label for="clientPassword">
                    <span class="span-label">Password:</span>
                    <input id="clientPassword" type="password" name="clientPassword" placeholder="Your Password" required />
                </label>
                <!-- <button type="button">Login </button> -->
                <button type="submit" name="submit" id="loginbtn" value="Login">Login</button>
                <!-- <input type="submit" name="submit" id="regbtn" value="Register" class="a-btn"> -->
                <!-- Add the action name - value pair -->
                <input type="hidden" name="action" value="Login">
            </fieldset>

            <fieldset>
                <legend>Not a Member?</legend>
                <br>
                <a href="../accounts/index.php?action=Registration" class="create-account">Create an account!</a>
            </fieldset>
        </form>
    </main>

    <footer>
        <?php include('../common/footer.php'); ?>
    </footer>
</body>

</html>
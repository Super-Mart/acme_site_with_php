<!DOCTYPE html>
<html lang="en">

<head>
    <title>Acme, Inc.</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="author" content="Erik Martinez">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:600%7CWendy+One" rel="stylesheet">
    <link rel="stylesheet" href="../css/main.css" media="screen" type="text/css">
    <link rel="stylesheet" href="../css/responsive.css" media="screen" type="text/css">
</head>

<body>
    <header id="site-brand-header">
        <?php include('../common/header.php'); ?>
    </header>

    <nav id="site-nav">
        <?php
        //include('./common/navigation.php'); 
        echo $navigationList
        ?>
    </nav>

    <main>
        <?php
        if (isset($message)) {
            echo $message;
        }
        ?>
        <form method="post" action="/acme/accounts/index.php" class="basic">
            <fieldset>

                <h1 class="acme-login-heading">Acme Registration</h1>

                <label for="clientFirstname">
                    <span class="span-label">First Name:</span>
                    <input id="clientFirstname" type="text" name="clientFirstname" pattern="[A-Za-z]{2,}" placeholder="" <?php if (isset($clientFirstname)) {
                                                                                                                                echo "value='$clientFirstname'";
                                                                                                                            } ?> required />
                </label>
                <label for="clientLastname">
                    <span class="span-label">Last Name:</span>
                    <input id="clientLastname" type="text" name="clientLastname" pattern="[A-za-z]{2,}" placeholder="" <?php if (isset($clientLastname)) {
                                                                                                                            echo "value='$clientLastname'";
                                                                                                                        } ?> required />
                </label>

                <label for="clientEmail">
                    <span class="span-label">Email:</span>
                    <input id="clientEmail" type="email" name="clientEmail" pattern="[a-z0-9._+-]+@[a-z0-9.-]+\.[a-z]{2,}$" placeholder="Your Email Address" <?php if (isset($clientEmail)) {
                                                                                                                                                                    echo "value='$clientEmail'";
                                                                                                                                                                } ?> required />
                </label>

                <label for="clientPassword">
                    <span class="span-label">Password:</span>
                    <input id="clientPassword" type="password" name="clientPassword" placeholder="Your Password" required />
                </label>

                <button type="submit" name="submit" id="regbtn" value="Register">Create account!</button>
                <!-- <input type="submit" name="submit" id="regbtn" value="Register" class="a-btn"> -->
                <!-- Add the action name - value pair -->
                <input type="hidden" name="action" value="register">
            </fieldset>
        </form>
    </main>

    <footer>
        <?php include('../common/footer.php'); ?>
    </footer>
</body>

</html>
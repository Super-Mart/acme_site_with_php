<?php
if (!$_SESSION['loggedin']) {
    header("location: /acme");
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Acme, Inc.</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="author" content="Erik Martinez">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:600%7CWendy+One" rel="stylesheet">
    <link rel="stylesheet" href="../css/main.css" media="screen" type="text/css">
    <link rel="stylesheet" href="../css/responsive.css" media="screen" type="text/css">
</head>

<body>
    <header id="site-brand-header">
        <div class="container">
            <div class="site-icon">
                <a href="../index.php"><img src="../images/site/logo.gif" alt="site logo" class="siteIcon"></a>
            </div>
            <div class="center">
            </div>
            <div class="account-icon">
                <?php if (isset($_SESSION['loggedin'])) {
                    echo "<a href='../accounts/index.php?action=LogOut'>LogOut</a>";
                    echo "<a class='welcomeCookie' href='../accounts/index.php?action=AdminPage'>Welcome " . $_SESSION['clientData']['clientFirstname'] . "</a>";
                } else {
                    echo "<a href='../accounts/index.php?action=MyAccount'><img src='../images/site/account.gif' alt='my account' class='accountIcon'> </a> <p class='myAccountText'>My Account</p>";
                } ?>

            </div>
        </div>
        <?php if (isset($cookieFirstname) && isset($_SESSION['loggedin'])) {
            echo "<a class='welcomeCookie' href='../accounts/index.php?action=AdminPage'>Welcome $cookieFirstname</a>";
        } else { } ?>
    </header>

    <nav id="site-nav">
        <?php
        //include('./common/navigation.php'); 
        echo $navigationList
        ?>
    </nav>

    <main>
        <?php
        if (isset($message)) {
            echo $message;
        }
        ?>
        <h1 class="welcome-user little-right">Welcome <?php echo $_SESSION['clientData']['clientFirstname']; ?>, you are now logged in.</h1>
        <ul class="list-container">
            <li>Firstname: <?php
                            echo $_SESSION['clientData']['clientFirstname'];
                            ?></li>
            <li>Lastname: <?php
                            echo $_SESSION['clientData']['clientLastname'];
                            ?></li>
            <li>Email: <?php
                        echo $_SESSION['clientData']['clientEmail'];
                        ?></li>
        </ul>
        <a href="../accounts/index.php?action=mod-account" class="update-account little-right">Update Account Information</a>

        <?php
        if ($_SESSION['clientData']['clientLevel'] == 3) {
            echo "<h2 class='little-right'>Administrative Fuctions</h2>";
            echo "<ul><li>Manage Products</li></ul>";

            echo "<div class='btn-con'><a href='../products/' class='a-btn'>Go to Products</a></div>";
        }
        ?>

    </main>

    <footer>
        <?php include('../common/footer.php'); ?>
    </footer>
</body>

</html>
<?php unset($_SESSION['message']); ?>
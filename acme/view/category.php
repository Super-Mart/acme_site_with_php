<!DOCTYPE html>
<html lang="en">

<head>
    <title><?php echo $categoryName; ?>
        Products | Acme, Inc.</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="author" content="Erik Martinez">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:600%7CWendy+One" rel="stylesheet">
    <link rel="stylesheet" href="../css/main.css" media="screen" type="text/css">
    <link rel="stylesheet" href="../css/responsive.css" media="screen" type="text/css">
</head>

<body>
    <header id="site-brand-header">
        <?php include('../common/header.php'); ?>
    </header>

    <nav id="site-nav">
        <?php
        echo $navigationList;
        ?>
    </nav>

    <main>
        <h1 class="little-right"><?php echo $categoryName; ?> Products</h1>
        <?php if (isset($message)) {
            echo $message;
        }
        ?>
        <?php if (isset($prodDisplay)) {
            echo $prodDisplay;
        } ?>
    </main>

    <footer>
        <?php include('../common/footer.php'); ?>
    </footer>
</body>

</html>
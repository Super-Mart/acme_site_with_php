<?php
if ($_SESSION['clientData']['clientLevel'] < 2) {
    header('location: /acme/');
    exit;
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>
        <?php if (isset($prodInfo['invName'])) {
            echo "Delete $prodInfo[invName]";
        } ?> | Acme, Inc.</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="author" content="Erik Martinez">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:600%7CWendy+One" rel="stylesheet">
    <link rel="stylesheet" href="../css/main.css" media="screen" type="text/css">
    <link rel="stylesheet" href="../css/responsive.css" media="screen" type="text/css">
</head>

<body>
    <header id="site-brand-header">
        <?php include('../common/header.php'); ?>
    </header>

    <nav id="site-nav">
        <?php
        //include('../common/navigation.php'); 
        echo $navigationList
        ?>
    </nav>

    <main>
        <?php
        if (isset($message)) {
            echo $message;
        }
        ?>
        <h1><?php if (isset($prodInfo['invName'])) {
                echo "Delete $prodInfo[invName]";
            } ?></h1>
        <p class="warning">Confirm Product Deletion. The delete is permanent.</p>
        <form method="post" action="/acme/products/" class="basic">
            <fieldset>
                <label for="invName">Product Name</label>
                <input type="text" readonly name="invName" id="invName" <?php if (isset($prodInfo['invName'])) {
                                                                            echo "value='$prodInfo[invName]'";
                                                                        } ?>>
                <label for="invDescription">Product Description</label>
                <textarea name="invDescription" readonly id="invDescription"><?php if (isset($prodInfo['invDescription'])) {
                                                                                    echo $prodInfo['invDescription'];
                                                                                } ?></textarea>
                <button type="submit" name="submit" id="deleteProd" value="deleteProd">Delete Product</button>
                <input type="hidden" name="action" value="deleteProd">
                <input type="hidden" name="invId" value="<?php if (isset($prodInfo['invId'])) {
                                                                echo $prodInfo['invId'];
                                                            } ?>">
            </fieldset>
        </form>
    </main>

    <footer>
        <?php include('../common/footer.php'); ?>
    </footer>
</body>

</html>
<?php
if ($_SESSION['clientData']['clientLevel'] < 2) {
    header("location: /acme/");
    exit;
}

if (isset($_SESSION['message'])) {
    $message = $_SESSION['message'];
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Image Management</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="author" content="Erik Martinez">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:600%7CWendy+One" rel="stylesheet">
    <link rel="stylesheet" href="../css/main.css" media="screen" type="text/css">
    <link rel="stylesheet" href="../css/responsive.css" media="screen" type="text/css">
</head>

<body>
    <header id="site-brand-header">
        <?php include('../common/header.php'); ?>
    </header>

    <nav id="site-nav">
        <?php
        echo $navList;
        ?>
    </nav>

    <main>
        <h1 class="little-right">Image Management</h1>
        <p class="little-right">Welcome to the image management page!</p>

        <h2 class="little-right">Add New Product Image</h2>
        <?php
        if (isset($message)) {
            echo $message;
        } ?>

        <form action="/acme/uploads/" method="post" class="basic" enctype="multipart/form-data">
            <label for="invItem">Product</label><br>
            <?php echo $prodSelect; ?><br><br>
            <label>Upload Image:</label><br>
            <input type="file" name="file1"><br>
            <input type="submit" class="regbtn" value="Upload">
            <input type="hidden" name="action" value="upload">
        </form>

        <hr>

        <h2 class="little-right">Existing Images</h2>
        <p class="notice">If deleting an image, delete the thumbnail too and vice versa.</p>
        <?php
        if (isset($imageDisplay)) {
            echo $imageDisplay;
        } ?>

    </main>

    <footer>
        <?php include('../common/footer.php'); ?>
    </footer>
</body>

</html>
<?php unset($_SESSION['message']); ?>
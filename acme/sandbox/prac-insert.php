<!DOCTYPE html>
<html lang="en">

<head>
    <title>Acme, Inc.</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="author" content="Erik Martinez">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:600%7CWendy+One" rel="stylesheet">
    <link rel="stylesheet" href="../css/main.css" media="screen" type="text/css">
    <link rel="stylesheet" href="../css/responsive.css" media="screen" type="text/css">
</head>

<body>
    <header id="site-brand-header">
        <?php include('../common/header.php'); ?>
    </header>

    <nav id="site-nav">
        <?php include('../common/navigation.php'); ?>
    </nav>

    <main>
        <?php
        if (isset($message)) {
            echo $message;
        }
        ?>
        <form method="post" action="/acme/sandbox/index.php" class="basic">
            <fieldset>

                <label for="pracTarget">Name of New Category:</label>
                <br>
                <input type="text" name="pracTarget" id="pracTarget">

                <button type="submit" name="submit" id="newCatbtn" value="pracTarget">Add New Target!</button>
                <!-- <input type="submit" name="submit" id="regbtn" value="Register" class="a-btn"> -->
                <!-- Add the action name - value pair -->
                <input type="hidden" name="action" value="pracTarget">
            </fieldset>
        </form>
    </main>

    <footer>
        <?php include('../common/footer.php'); ?>
    </footer>
</body>

</html>
<?php
/*
* Products Controller
*/

//Start session
session_start();

// Get the database connection file
require_once '../library/connections.php';
// Get the acme model for use as needed
require_once '../model/acme-model.php';
// Get the sandbox model
require_once '../model/prac-model.php';

// Get the array of categories
$categories = getCategories();
$practiceTarget = getTargets();
//var_dump($categories);

// Build a navigation bar using the $categories array
$navList = '<ul class="nav-container">';
$navList .= "<li><a href='/acme/index.php' title='View the Acme home page'>Home</a></li>";
foreach ($categories as $category) {
    $navList .= "<li><a href='/acme/index.php?action=urlencode($category[categoryName])' title='View our $category[categoryName] product line'>$category[categoryName]</a></li>";
}
$navList .= '</ul>';

// Build list for practice table
$targetList = '<ul id="targets">';
foreach ($practiceTarget as $practice) {
    $targetList .= "<li>$practice[pracTarget]</li>";
}
$targetList .= '</ul>';


// Get the value from the action name - value pair
$action = filter_input(INPUT_POST, 'action');
if ($action == NULL) {
    $action = filter_input(INPUT_GET, 'action');
}
switch ($action) {
        // Code to deliver the views will be here
    case 'prac-insert':
        include '../view/prac-insert.php';
        break;
    case 'prac-view':
        include '../view/prac-view.php';
        break;
    case 'pracTarget':
        // Filter and store the data
        $pracTarget = filter_input(INPUT_POST, 'pracTarget');

        // echo "$pracTarget";
        // exit;

        // Check for missing data
        if (empty($pracTarget)) {
            $message = '<p>Please provide information for all empty form fields.</p>';
            include '../view/prac-insert.php';
            exit;
        }

        // Send the data to the model
        $newTargetOutcome = addNewTarget($pracTarget);

        // Check and report the result
        if ($newTargetOutcome === 1) {
            $message = "<p>$pracTarget has been added to Products.</p>";
            include '../view/prac-insert.php';
            exit;
        } else {
            $message = "<p>Sorry, but the $invName cannot be added. Please try again.</p>";
            include '../view/prac-insert.php';
            exit;
        }
        // case 'addNewCategory':
        // Filter and store the data
        // $categoryName = filter_input(INPUT_POST, 'categoryName');

        // echo "$categoryName";
        // exit;

        // Check for missing data
        // if (empty($categoryName)) {
        //     $message = '<p>*Please provide information for all empty form fields.</p>';
        //     include '../view/new-prod.php';
        //     exit;
        // }

        // Send the data to the model
        // $newCatOutcome = addNewCategory(
        //     $categoryName
        // );

        // Check and report the result
        // if ($newCatOutcome === 1) {
        //     $message = "<p>$categoryName has been added to Categories.</p>";
        //     include '../view/new-cat.php';
        //     exit;
        // } else {
        //     $message = "<p>Sorry, but $categoryName cannot be added.</p>";
        //     include '../view/new-cat.php';
        //     exit;
        // }

    default:
        include '../view/prac-mgmt.php';
        exit;
}

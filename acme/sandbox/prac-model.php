<?php
/* Sandbox Model */
function addNewTarget($pracTarget)
{
    // Create a connection object using the acme connection function
    $db = acmeConnect();
    // The SQL statement    
    $sql = 'INSERT INTO practice
    (pracTarget)
     VALUES (:pracTarget)';
    // Create the prepared statement using the acme connection
    $stmt = $db->prepare($sql);
    // The next four lines replace the placeholders in the SQL
    // statement with the actual values in the variables
    // and tells the database the type of data it is
    $stmt->bindValue(':pracTarget', $pracTarget, PDO::PARAM_STR);
    // Insert the data
    $stmt->execute();
    // Ask how many rows changed as a result of our insert
    $rowsChanged = $stmt->rowCount();
    // Close the database interaction
    $stmt->closeCursor();
    // Return the indication of success (rows changed)
    return $rowsChanged;
}
function getTargets()
{
    $db = acmeConnect(); // The SQL statement to be used with the database 
    $sql = 'SELECT * FROM practice ORDER BY pracTarget ASC';
    // The next line creates the prepared statement using the acme connection
    // The next line runs the prepared statement 
    $stmt = $db->prepare($sql);
    $stmt->execute(); // The next line gets the data from the database and 
    // stores it as an array in the $categories variable 
    $practice = $stmt->fetchAll();
    // The next line closes the interaction with the database 
    $stmt->closeCursor();
    // The next line sends the array of data back to where the function 
    // was called (this should be the controller) 
    return $practice;
}

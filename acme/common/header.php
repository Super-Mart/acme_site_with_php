<div class="container">
    <div class="site-icon">
        <a href="../index.php"><img src="../images/site/logo.gif" alt="site logo" class="siteIcon"></a>
    </div>
    <div class="center">
    </div>
    <div class="account-icon">
        <?php if (isset($_SESSION['loggedin'])) {
            echo "<a href='../accounts/index.php?action=LogOut'>LogOut</a>";
            echo "<a class='welcomeCookie' href='../accounts/index.php?action=AdminPage'>Welcome " . $_SESSION['clientData']['clientFirstname'] . "</a>";
        } else {
            echo "<a href='../accounts/index.php?action=MyAccount'><img src='../images/site/account.gif' alt='my account' class='accountIcon'> </a> <p class='myAccountText'>My Account</p>";
        } ?>

    </div>
</div>
<?php if (isset($cookieFirstname) && isset($_SESSION['loggedin'])) {
    echo "<a class='welcomeCookie' href='../accounts/index.php?action=AdminPage'>Welcome $cookieFirstname</a>";
} else { } ?>
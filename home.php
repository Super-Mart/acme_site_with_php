<!DOCTYPE html>
<html lang="en">

<head>
    <title>Home</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="author" content="Erik Martinez">
    <link rel="stylesheet" href="./modularize/styles/main.css" media="screen" type="text/css">
</head>

<body>
    <header id="site-brand-header">
        <p id="site-brand">Home Page</p>
    </header>

    <nav id="site-nav">
        <?php include('./modularize/navigation.php'); ?>
    </nav>

    <main>

    </main>

    <footer>

    </footer>
</body>

</html>